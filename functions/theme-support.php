<?php
/**
 * Theme support.
 *
 * @package dax_blank
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! function_exists( 'dax_blank_child_theme_support' ) ) :

	function dax_blank_child_theme_support() {

		// Child Theme Textdomain
		load_child_theme_textdomain( 'dax_blank_child', get_stylesheet_directory() . '/languages' );

		// Add styles.css as editor style https://codex.wordpress.org/Editor_Style
		add_editor_style( '/css/dax_blank_child.css' );

	}
	add_action( 'after_setup_theme', 'dax_blank_child_theme_support' );

endif; // Ends if theme support function exists.
