<?php
/**
 * Register extra menus and functions to call them.
 *
 * @package dax_blank_child
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! function_exists( 'dax_blank_child_menus' ) ) :

	function dax_blank_child_menus() {
		$locations = array(
			//'footer' => __( 'Footer Menu' ),
			//'other' => __( 'Other Menu' ),
		);
		register_nav_menus( $locations );
	}

  add_action( 'init', 'dax_blank_child_menus' );

endif; // Ends if Menu function exists.
