<?php
/**
 * Enqueue scripts.
 *
 * @package dax_blank_child
 */

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'wp_enqueue_scripts', 'dax_blank_child_scripts', 13);
add_action( 'login_enqueue_scripts', 'dax_blank_child_register_styles', 10 ); // Add our styes to Login page.


function dax_blank_child_scripts() {

	wp_enqueue_style( 'dax_blank_child_style', get_stylesheet_directory_uri() . '/assets/child.css', false, 1.0.0 );

	wp_enqueue_script( 'dax_blank_child_scripts', get_stylesheet_directory_uri() . '/assets/js/child.js', false, 1.0.0, true );

  // Add the comment-reply library on pages where it is necessary.
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
