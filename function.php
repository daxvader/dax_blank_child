<?php
/**
 * Child Theme support.
 *
 * @package dax_blank_child
 */

if ( ! defined( 'ABSPATH' ) ) exit;

// Add theme support.
require_once( get_stylesheet_directory().'/functions/theme-support.php' );

// Enqueue scripts.
require_once( get_stylesheet_directory().'/functions/enqueue.php' );

// Register menus and menu walkers.
require_once( get_stylesheet_directory().'/functions/menu.php' );
